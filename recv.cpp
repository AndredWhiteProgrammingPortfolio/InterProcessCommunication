/* Andre White
   CS351 Section 3
   Assignment 1
   recv.cpp
*/
#include <sys/shm.h>
#include <sys/msg.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "msg.h"    /* For the message struct */


/* The size of the shared memory chunk */
#define SHARED_MEMORY_CHUNK_SIZE 1000

/* The ids for the shared memory segment and the message queue */
int shmid, msqid;

/* The pointer to the shared memory */
void *sharedMemPtr;

/* The name of the received file */
const char recvFileName[] = "recvfile";


/**
 * Sets up the shared memory segment and message queue
 * @param shmid - the id of the allocated shared memory 
 * @param msqid - the id of the shared memory
 * @param sharedMemPtr - the pointer to the shared memory
 */

void init(int& shmid, int& msqid, void*& sharedMemPtr)
{
	//buffer for key
	key_t key;
	//creating key
	if((key=ftok("keyfile.txt",'R'))==-1){
		perror("ftok");
		exit(1);
	}
	//creating message queue
	if((msqid=msgget(key,0666|IPC_CREAT))==-1){
		perror("mssget");
		exit(1);
	}
	//creating shared memory segment
	if((shmid=shmget(key, SHARED_MEMORY_CHUNK_SIZE, 0644|IPC_CREAT))==-1){
		perror("shmget");
		exit(1);
	}
	//attach to shared memory segment
	 sharedMemPtr = shmat(shmid, (void *)0, 0);
   	 if (sharedMemPtr == (char *)(-1)) {
        	perror("shmat");
        	exit(1);
    }
}
 

/**
 * The main loop
 */
void mainLoop()
{	//buffer struct for receiving msg
	message recvmsg;
	recvmsg.mtype=SENDER_DATA_TYPE;
	//buffer struct for sending msg
	message sndmsg;
	//set the message type for sending msg	
	sndmsg.mtype=RECV_DONE_TYPE;
	/* The size of the mesage */
	int msgSize = 0;
	
	/* Open the file for writing */
	FILE* fp = fopen(recvFileName, "w");
		
	/* Error checks */
	if(!fp)
	{
		perror("fopen");	
		exit(-1);
	}
	else{
		puts( "File Open \n");
	}
	/* Keep receiving until the sender set the size to 0, indicating that
 	 * there is no more data to send
 	 */
	while(1){	
		if(msgrcv(msqid, &recvmsg, sizeof(struct message), -2, 0)==-1){
			perror("msgrcv");
			exit(1);
		}
		else{
			puts( "Message Received");
		}
		/* If the sender is not telling us that we are done, then get to work */
		if(recvmsg.size != 0)
		{
			/* Save the shared memory to file */
			if(fwrite(sharedMemPtr, sizeof(char), recvmsg.size, fp) < 0)
			{
				perror("fwrite");
			}
			else{
				printf("wrote %i bytes of data\n",recvmsg.size);
			}
			if(msgsnd(msqid, &sndmsg, sizeof(struct message),0)==-1){
				perror("msgsnd");
				exit(1);
			}
			else{
				puts("Sent Message");
			}
		}
		else
		{
			/* Close the file */
			fclose(fp);
			puts("File Closed");
			break;
		}
	}
	printf("%s", "Done");
}



/**
 * Perfoms the cleanup functions
 * @param sharedMemPtr - the pointer to the shared memory
 * @param shmid - the id of the shared memory segment
 * @param msqid - the id of the message queue
 */

void cleanUp(const int& shmid, const int& msqid, void* sharedMemPtr)
{
	/*Detach from shared memory */
	if(shmdt(sharedMemPtr)==-1){
		perror("shmdt");
		exit(1);
	}
	/*Deallocate the shared memory chunk */
	shmctl(shmid, IPC_RMID,NULL);
	
	/*Deallocate the message queue */
	msgctl(msqid, IPC_RMID, NULL);
}

/**
 * Handles the exit signal
 * @param signal - the signal type
 */

void ctrlCSignal(int signal)
{
	/* Free system V resources */
	cleanUp(shmid, msqid, sharedMemPtr);
}

int main(int argc, char** argv)
{
	
	//signal Handler
	signal(SIGINT, ctrlCSignal);		
	/* Initialize */
	init(shmid, msqid, sharedMemPtr);
	
	/* Go to the main loop */
	mainLoop();

	cleanUp(shmid, msqid, sharedMemPtr);
		
	return 0;
}
