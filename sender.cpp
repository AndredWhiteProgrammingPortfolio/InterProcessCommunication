/* Andre White
   CS351 Section 3
   Assignment 1
   sender.cpp
*/
#include <sys/shm.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "msg.h"    /* For the message struct */

/* The size of the shared memory chunk */
#define SHARED_MEMORY_CHUNK_SIZE 1000

/* The ids for the shared memory segment and the message queue */
int shmid, msqid;

/* The pointer to the shared memory */
void* sharedMemPtr;

/**
 * Sets up the shared memory segment and message queue
 * @param shmid - the id of the allocated shared memory 
 * @param msqid - the id of the shared memory
 */

void init(int& shmid, int& msqid, void*& sharedMemPtr)
{
	key_t key;
	//creating key
	if((key=ftok("keyfile.txt",'R'))==-1){
		perror("ftok");
		exit(1);
	}
	//creating message queue
	if((msqid=msgget(key,0666|IPC_CREAT))==-1){
		perror("mssget");
		exit(1);
	}
	//creating shared memory segment
	if((shmid=shmget(key, SHARED_MEMORY_CHUNK_SIZE, 0644|IPC_CREAT))==-1){
		perror("shmget");
		exit(1);
	}
	//attach to shared memory segment
	 sharedMemPtr = shmat(shmid, (void *)0, 0);
   	 if (sharedMemPtr == (char *)(-1)) {
        	perror("shmat");
        	exit(1);
	}
}

/**
 * Performs the cleanup functions
 * @param sharedMemPtr - the pointer to the shared memory
 * @param shmid - the id of the shared memory segment
 * @param msqid - the id of the message queue
 */

void cleanUp(const int& shmid, const int& msqid, void* sharedMemPtr)
{
	/*Detach from shared memory */
	if(shmdt(sharedMemPtr)==-1){
		perror("shmdt");
		exit(1);
	}
}

/**
 * The main send function
 * @param fileName - the name of the file
 */
void send(const char* fileName)
{
	/* Open the file for reading */
	FILE* fp = fopen(fileName, "r");
	

	/* A buffer to store message we will send to the receiver. */
	message sndMsg; 
	sndMsg.mtype=SENDER_DATA_TYPE;
	/* A buffer to store message received from the receiver. */
	message rcvMsg;
	
	/* Was the file open? */
	if(!fp)
	{
		perror("fopen");
		exit(-1);
	}
	else{
		puts("File open");
	}
	
	/* Read the whole file */
	while(!feof(fp))
	{
		/* Read at most SHARED_MEMORY_CHUNK_SIZE from the file and store them in shared memory. 
 		 * fread will return how many bytes it has actually read (since the last chunk may be less
 		 * than SHARED_MEMORY_CHUNK_SIZE).
 		 */
		if((sndMsg.size = fread(sharedMemPtr, sizeof(char), SHARED_MEMORY_CHUNK_SIZE, fp)) < 0)
		{
			perror("fread");
			exit(-1);
		}
		else{
			printf("Read %i bytes of data \n", sndMsg.size);
		
		}
		if(msgsnd(msqid, &sndMsg, sizeof(struct message),0)==-1){
			perror("msgsnd");
			exit(1);
		}
		else{
			puts("Sent Message");
		}
		if (msgrcv(msqid, &rcvMsg, sizeof (struct message),0,0) == -1) {
           		perror("msgrcv");
            		exit(1);
      		}
		
		
		
	}
	sndMsg.size=0;
	sndMsg.mtype=RECV_DONE_TYPE;
	if(msgsnd(msqid, &sndMsg, sizeof(struct message),0)==-1){
		perror("msgsnd");
		exit(1);
	}
	else
		puts("Done Sending file");
		
	/* Close the file */
	fclose(fp);
	puts("File closed");
	
}


int main(int argc, char** argv)
{
	
	/* Check the command line arguments */
	if(argc < 2)
	{
		fprintf(stderr, "USAGE: %s <FILE NAME>\n", argv[0]);
		exit(-1);
	}
		
	/* Connect to shared memory and the message queue */
	init(shmid, msqid, sharedMemPtr);
	
	/* Send the file */
	send(argv[1]);
	
	/* Cleanup */
	cleanUp(shmid, msqid, sharedMemPtr);
		
	return 0;
}
